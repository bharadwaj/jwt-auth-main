package com.experiment.authmain.security.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}