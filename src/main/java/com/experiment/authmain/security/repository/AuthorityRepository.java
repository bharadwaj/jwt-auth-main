package com.experiment.authmain.security.repository;

import com.experiment.authmain.security.model.Authority;
import org.springframework.data.repository.CrudRepository;

public interface AuthorityRepository extends CrudRepository<Authority, Long> {
}
